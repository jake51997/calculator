<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('calculator', function () {
    return view('calculator');
});

Route::get('/', 'HomeController@index');
Route::get('main', 'MainController@index');
Route::post('/main/checklogin','MainController@checklogin');
Route::get('/main/calculator2', 'MainController@calculator2');
Route::get('/main/logout', 'MainController@logout');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
