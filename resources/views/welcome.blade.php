<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <style>
        div {
            width: 300px;
            margin-left: 35%;
            margin-right: 35%;
            margin-top:100px;
            border: solid 1px;
            background-color: #20c997;
            border-radius: 10px;
            padding: 90px 20px 50px 80px;
            height: 200px;
        }
        @media only screen and (max-width: 600px) {

            div {
                width: 300px;
                margin-left: 0%;
                background-color: #dc3545;
                height: 200px;
            }
        }
    </style>
</head>
<body>
<div>
<form id='login' action="{{url('/main/checklogin')}}" method='post' accept-charset='UTF-8'>
    <table>
        <tr>
        <td colspan="12" style="text-align: center; font-size: 30px; margin-left: 30px;"><legend>Login</legend></td>
        </tr>
        <input type='hidden' name='submitted' id='submitted' value='1'/>
        @if(isset(Auth::user()->email))
            <script>window.location="/main/calculator2";</script>
            @endif
        @if (count($errors)>0)
            <div>
                <ul>
                    @foreach($errors->all() as $errors)
                        <li>{{$errors}}</li>
                        @endforeach
                </ul>
            </div>
        @endif
        <tr>
       <td><label for='username' >EMAIL*:</label></td>
            <td><input type='email' name='email' id='email'  maxlength="50" style="height: 20px; border-radius: 5px; margin-top: 10px; margin-bottom: 10px; font-size:13px; width:150px;" /></td>
        </tr>

        <tr>
            <td><label for='password' >Password*:</label></td>
            <td><input type='password' name='password' id='password' maxlength="50" style="height: 20px; border-radius: 5px; margin-top: 10px; margin-bottom: 10px; font-size: 13px; width:150px;" /></td>
        </tr>

        <tr>
            <td colspan="12"><input type='submit' name='Submit' value='Submit' style="margin-left: 100px; margin-top: 20px; font-size: 50px; "  /></td>

        </tr>
    </table>
</form>
</div>

</body>
</html>