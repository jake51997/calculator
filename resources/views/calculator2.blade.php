<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>PHP calculator</title>
    <style>
        div {
            width: 430px;
            margin-left: 35%;
            margin-right: 35%;
            border: solid 1px;
            background-color: #20c997;
            border-radius: 10px;
            padding-bottom: 5px;
            height: 650px;
        }

        @media only screen and (max-width: 600px) {

            div {
                width: 100%;
                margin-left: 0%;
                background-color: #dc3545;
                height: 615px;

            }
        }
    </style>
</head>
<body>
<a href="{{ route('logout') }}"
   onclick="event.preventDefault();
    document.getElementById('logout-form').submit();">
    {{ __('Logout') }}
</a>

<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    @csrf
</form>
<a href="calculator" style="float: right;">Javascript Version</a>

<div>
  <?php

    if (isset($_GET['Equal'])) {
        $input1 = $_GET['num1'];
        $input2 = $_GET['num2'];
        $operator = $_GET['operator'];
        if (!is_numeric($input1) && !is_numeric($input2)) {
            $answer = "";

        }else if(!is_numeric($input2) || $operator=="None" || $input2==""){
            $answer= $input1;
        }else {
            if ($operator == "None") {
                $answer = "Error";
            } elseif ($operator == "+") {
                $answer = add($input1, $input2);
                $answer= rtrim(rtrim((string)number_format($answer, 2, ".", ","),"0"),".");
            } elseif ($operator == "-") {
                $answer = minus($input1, $input2);
                $answer= rtrim(rtrim((string)number_format($answer, 2, ".", ","),"0"),".");
            } elseif ($operator == "x") {
                $answer = multiply($input1, $input2);
                $answer= rtrim(rtrim((string)number_format($answer, 2, ".", ","),"0"),".");
            } else {
                if ($input1 == "0" || $input2 == "0") {
                    $answer = "Infinity";
                }else{
                    $answer = divide($input1, $input2);
                    $answer= rtrim(rtrim((string)number_format($answer, 2, ".", ","),"0"),".");
                }
            }
        }
        if ($answer != null) {
            $input1 = $answer;
        }
    } elseif (isset($_GET['clear'])) {
        $input1 = "";
        $input2 = "";
        $answer = "";
        $operator = "";
    }
    function add($input1, $input2)
    {$input1 = floatval(str_replace(",","",$input1));
        $add = $input1 + $input2;

        return $add;
    }
    function minus($input1, $input2)
    {
        $input1 = floatval(str_replace(",","",$input1));
        $minus = $input1 - $input2;

        return $minus;
    }
    function multiply($input1, $input2)
    {
        $input1 = floatval(str_replace(",","",$input1));
        $multiply = $input1 * $input2;

        return $multiply;
    }
    function divide($input1, $input2)
    {
        $input1 = floatval(str_replace(",","",$input1));    
        $divide = $input1 / $input2;

        return $divide;
    }

    ?>
    <form method="get">
        <table>
            <tr>
                <td colspan="12"><input value='<?php if (isset($answer)) {echo $answer;} ?>' type="text" name="display" id="display" placeholder="Answer" style="border: solid 1px; border-radius: 15px; width: 100%; height: 100px; font-size: 50px; font-weight: normal; text-align:center" disabled></td>
            </tr>
            <tr>
                <td colspan="6"><input value='<?php if (isset($answer)) {echo $answer;} ?>' type="text" name="num1" placeholder="Input 1" maxlength="9" style="border: solid 1px; border-radius:15px; width:100%; height:50px; font-size: 30px; font-weight: normal; text-align: center; margin-top: 50px;">
                </td>
            </tr>
            <tr>
                <td colspan="12"><select name="operator"
                                         style="border: solid 1px; border-radius: 15px; width: 100%; height: 80px; font-size: 50px; font-weight: normal; text-align:center; margin-top: 5px">
                        <option>None</option>
                        <option>+</option>
                        <option>-</option>
                        <option>x</option>
                        <option>÷</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td colspan="6"><input type="text" name="num2" placeholder="Input 2" maxlength="9" style="border: solid 1px; border-radius:15px; width:100%; height:50px; font-size: 30px; font-weight: normal; text-align: center; margin-top: 5px">
                </td>
            </tr>
            <tr>
                <td>
                    <button type="submit" name="Equal" value="Equal" style="border: solid 1px; border-radius:15px; width: 200px; height:50px; font-size: 30px; font-weight: normal; text-align: center; margin-top: 20px">=</button>
                </td>
                <td>
                    <button type="submit" name="clear" value="clear" style="border: solid 1px; border-radius:15px; width: 200px; height:50px; font-size: 30px; font-weight: normal; text-align: center; margin-top: 20px">AC</button>
                </td>
            </tr>

        </table>
    </form>
</div>
</body>
</html>
