<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <title>Calculator</title>
    <style>

        div{
            width: 430px;
            margin-left: 35%;
            margin-right: 35%;
            border: solid 1px;
            background-color: #20c997;
            border-radius: 10px;
            padding-bottom: 5px;
            height: 650px;
        }
        @media only screen and (max-width:600px) {

            div {
                width: 100%;
                margin-left: 0%;
                background-color: #dc3545;
                height: 615px;

            }
        }
    </style>
</head>
<body>
<a href="/">Back</a>
<div >

    <form name="calculator">


        <table >
            <tr>
                <td colspan="12">
                    <input style="border: solid 1px; border-radius: 15px; width: 100%; height: 100px; font-size: 50px; font-weight: normal; text-align: right" type="text" name="display"  id="display" placeholder="0"disabled>
                </td>
            </tr>
            <tr>
                <td><input onclick="btninput(1)" style="border: none; width: 100px;  font-size: 60px;  height: 100px;" type="button" name="one" value="1" ></td>
                <td><input onclick="btninput(2)" style="border: none; width: 100px;  font-size: 60px;  height: 100px;" type="button" name="two" value="2"></td>
                <td><input onclick="btninput(3)" style="border: none; width: 100px;  font-size: 60px;  height: 100px;" type="button" name="three" value="3"></td>
                <td><input onclick="btninput('+')" style="border: none; width: 100px;  font-size: 60px;  height: 100px;" type="button" name="plus" value="+" id="plus"></td>
            </tr>
            <tr>
                <td><input onclick="btninput(4)" style="border: none; width: 100px;  font-size: 60px;  height: 100px;" type="button" name="four" value="4" ></td>
                <td><input onclick="btninput(5)" style="border: none; width: 100px;  font-size: 60px;  height: 100px;" type="button" name="five" value="5" ></td>
                <td><input onclick="btninput(6)" style="border: none; width: 100px;  font-size: 60px;  height: 100px;" type="button" name="six" value="6" ></td>
                <td><input onclick="btninput('-')" style="border: none; width: 100px;  font-size: 60px;  height: 100px;" type="button" name="minus" value="-" id="minus"></td>
            </tr>
            <tr>
                <td><input onclick="btninput(7)" style="border: none; width: 100px; padding: 10px; font-size: 60px;  height: 100px;" type="button" name="seven" value="7" ></td>
                <td><input onclick="btninput(8)" style="border: none; width: 100px; padding: 10px; font-size: 60px;  height: 100px;" type="button" name="eight" value="8" ></td>
                <td><input onclick="btninput(9)" style="border: none; width: 100px; padding: 10px; font-size: 60px;  height: 100px;" type="button" name="nine" value="9" ></td>
                <td><input onclick="btninput('*')" style="border: none; width: 100px; padding: 10px; font-size: 60px;  height: 100px;" type="button" name="multiply" value="*" id="multiply"></td>
            </tr>
            <tr>
                <td><input onclick="btnclear()" style="border: none; width: 100px; padding: 10px; font-size: 60px;  height: 100px;" type="button" id="clear" name="clear" value="AC"></td>
                <td><input onclick="btninput(0)" style="border: none; width: 100px; padding: 10px; font-size: 60px;  height: 100px;" type="button" name="zero" value="0" id="zero" ></td>
                <td><input onclick="btninput('.')" style="border: none; width: 100px; padding: 10px; font-size: 60px;  height: 100px;" type="button" name="dot" value="." id="dot"></td>
                <td><input onclick="btninput('/')" style="border: none; width: 100px; padding: 10px; font-size: 60px;  height: 100px;" type="button" name="divide" value="÷" id="divide"></td>
            </tr>
            <tr>
                <td colspan="12">
                    <input onclick="btnequal()" style=" border: none; width: 100%; font-size: 60px; height: 90px;" type="button" id="total" value="=" id="equal">
                </td>
            </tr>
        </table>
    </form>
</div>
<script>

    function btninput(num) {

        var check = document.getElementById("display");
        var lastInput = check.value.substr(check.value.length - 1);

        if (check.value=="" && num=="+" ||check.value=="" && num== "-"
            ||check.value=="" && num =="*" ||check.value=="" && num =="/" || check.value=="" && num=="." || check.value=="0" && num=="0") {

            btnclear();
        }
        else if (isNaN(num) &&
            (lastInput === '.' || lastInput === '+' || lastInput === '-' || lastInput === '*' || lastInput === '/' || lastInput=='zero'|| lastInput=="equal")) {
            document.getElementById('plus').disabled = true;
            document.getElementById('minus').disabled = true;
            document.getElementById('multiply').disabled = true;
            document.getElementById('divide').disabled = true;
            document.getElementById('dot').disabled = true;
            document.getElementById('zero').disabled = true;
            document.getElementById('equal').disabled = true;
            alert("Error!!! All clear now");
            btnclear();
        } else {
            document.getElementById('plus').disabled = false;
            document.getElementById('minus').disabled = false;
            document.getElementById('multiply').disabled = false;
            document.getElementById('divide').disabled = false;
            document.getElementById('dot').disabled = false;
            document.getElementById('zero').disabled = false;
            document.calculator.display.value = document.calculator.display.value + num;
        }
    }
    function btnclear() {

        document.calculator.display.value ="";
    }
    function btnequal() {
        var answer = document.calculator.display.value;
        answer = eval(answer);
        answer = answer.toFixed(2);
        answer = addCommas(answer);
        document.calculator.display.value = answer;


    }
    function addCommas(nStr) {
        var num1;
        var num2;
        var r;
        var answer;

        nStr += '';
        r = nStr.split('.');
        num1 = r[0];
        num2 = r.length > 1 ? '.' + r[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(num1)) {
            num1 = num1.replace(rgx, '$1' + ',' + '$2');
        }
        answer = num1 + num2;
        return answer;
    }


</script>
</body>
</html>